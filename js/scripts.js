var dino = [];
var cont = 0;
var cont_frames = 0;
var delay = 10;

function setup() {
    createCanvas(1300, 600);
    for (let i = 1; i <= 10; i++) {
    dino.push(loadImage("imgs/Walk ("+i+").png"));
    }
}

function draw() {
    cont_frames++;
    if (!(cont_frames % delay)){
        background(220);
        dinoImg = dino[cont];
        image(dinoImg, 10 ,10);
        
        cont++;
        if (cont >= 10) {
            cont = 0;
        }
    }

}
